<!DOCTYPE html>
<html>
<head>
    <title>Пример адаптивной вёрстки макета. Heaven Web - блог для веб мастера.</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <link rel="stylesheet" href="<?php echo $url->get_url(); ?>css/style.css">
    <script src="<?php echo $url->get_url(); ?>js/jquery-2.0.3.min.js"></script>
    <script src="<?php echo $url->get_url(); ?>js/sstu_script.js"></script>
</head>
<body>
<div class="left_side">
    <h1 class="logo_text">
        <a href="/"><img src="<?php echo $url->get_url(); ?>images/logo.png" height="150" width="177" alt=""></a>
        <span>Блог о веб разработке</span>
    </h1>
    <div class="social_img">
        <a href="#" class="tw_icon"></a>
        <a href="#" class="go_icon"></a>
    </div>
    <div class="left_menu">
        <ul>
            <li><a href="index.html" id="link_top">Главная</a></li>
            <li><a href="verstka">Вёрстка</a></li>
            <li><a href="index.html">Jquery</a></li>
            <li><a href="index.html">Drupal</a></li>
            <li><a href="index.html">Другое</a></li>
            <li><a href="index.html">Контакты</a></li>
        </ul>
    </div>
</div>
<a href="#" class="left_swap"></a>