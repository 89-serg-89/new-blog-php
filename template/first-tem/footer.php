<div class="footer">
    <div class="footer_block">
        <h4><a href="#">Первый заголовок</a></h4>
        <img src="<?php echo $url->get_url(); ?>images/img_min_1.png" alt="">
        <p>При этом построение бренда упорядочивает сублимированный опрос, учитывая результат предыдущих медиа-кампаний.</p>
    </div>
    <div class="footer_block">
        <h4><a href="#">Второй заголовок</a></h4>
        <img src="<?php echo $url->get_url(); ?>images/img_min_2.png" alt="">
        <p>Исходя из структуры пирамиды Маслоу, лидерство в продажах упорядочивает принцип восприятия, отвоевывая свою долю рынка.</p>
    </div>
    <div class="footer_block">
        <h4><a href="#">Третий заголовок</a></h4>
        <img src="<?php echo $url->get_url(); ?>images/img_min_3.png" alt="">
        <p>Повышение жизненных стандартов, как следует из вышесказанного, развивает формирование имиджа, не считаясь с затратами.</p>
    </div>
</div>
</div>
</body>
</html>