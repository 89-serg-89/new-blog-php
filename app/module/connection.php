<?php
    require_once ($_SERVER['DOCUMENT_ROOT'] . '/app/config/config.php');

    class Db {
        private static $connection = null;

        public static function getConnection($host,$name,$user,$password)
        {
            if (self::$connection == null) {
                self::$connection = new PDO(
                    "mysql:host=$host;
                    dbname=$name",
                    $user,
                    $password,
                    [
                        \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                        \PDO::ATTR_TIMEOUT => 180,
                    ]
                );

            }
            return self::$connection;
        }

        public function selectPost($sql) {
            $query = self::$connection->query($sql);
            $row = $query->fetchAll(PDO::FETCH_ASSOC);
            foreach ($row as $res) {
                echo '<div class="text_block">';
                echo $res['text'];
                echo '</div>';
            }
        }

        public function updatePost($textPost) {
            $query = self::$connection->prepare("UPDATE `posts` SET text = :text WHERE id = '3'");
            $query->execute(['text' => $textPost]);
        }
    }
    $connect = Db::getConnection($config['db_host'], $config['db_name'], $config['db_us_name'], $config['db_us_pasw']);
    $Db = new Db;
/*$stmt = new Db;
$stmt->S1elect("SELECT * FROM `posts`");
    $query = $connect->query("SELECT * FROM `posts`");
$res = $query->fetchAll(PDO::FETCH_ASSOC);
foreach ($res as $row) {
    echo $row['id'];
}*/
/*echo '<pre>';
print_r($res);
echo '</pre>';*/
