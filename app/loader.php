<?php
    require_once ('config/config.php');

    class url_theme {
        private $theme;
        private $link;
        function __construct($theme)
        {
            $this->theme = $theme;
            return $this->get_url();
        }

        function get_url() {
            return $this->link = 'template/' . $this->theme . '/';
        }
    }
    $url = new url_theme(THEME);