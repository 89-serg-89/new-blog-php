<?php
    require_once ('app/module/connection.php');
    $Db->selectPost("SELECT * FROM `posts`");
    //print_r($_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']);
    $textPost = $_POST['txt'];
    if(isset($_POST['update'])) {
        $Db->updatePost($textPost);
        header("location:" . $_SERVER['REQUEST_URI']);
    }
?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>админ-панель</title>

    <script src="/includes/js/jquery/jquery-2.0.3.min.js"></script>
    <script src="/includes/ckeditor_4.6.2_full/ckeditor/ckeditor.js"></script>
    <script src="/includes/AjexFileManager/ajex.js"></script>

</head>
<body>
<form action="#" method="POST">
    <textarea id="editor" name="txt" cols="30" rows="10"><?php echo $Db->text ?></textarea>
    <script type="text/javascript">
        var ckeditor1 = CKEDITOR.replace( 'editor' );
        AjexFileManager.init({
            returnTo: 'ckeditor',
        editor: ckeditor1
        });
    </script>
    <input type="submit" name="update" value="Обновить">
</form>
</body>
</html>
